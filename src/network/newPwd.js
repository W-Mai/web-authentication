import {
  POST
} from './request.js';

export function newPwd(data) {
  return POST({
    url: "/v1/admin/newPwd",
    data: {
      oldPwd: data.oldPwd,
      newPwd: data.newPwd
    }
  })
}
