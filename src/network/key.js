import {
  POST,
  GET
} from './request.js';

export function getAll(data) {
  return GET({
    url: "/v1/key/getAll",
    params: {
      page: data.page,
      limit: data.limit,
      aid: data.aid,
      state: data.state,
      cdk: data.cdk
    }
  })
}
export function edit(data) {
  return POST({
    url: "/v1/key/edit",
    data: {
      kid: data.kid,
      state: data.state
    }
  })
}

export function Cread(data) {  
  return POST({
    url: "/v1/key/cread",
    data: {
      num: data.num,
      mold: data.mold,
      point: data.point,
      day: data.day,
      aid: data.aid
    }
  })
}