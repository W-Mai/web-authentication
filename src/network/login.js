import {
  POST
} from './request.js';

export function login(data) {
  return POST({
    url: "/v1/admin/login",
    data: {
      user: data.user,
      pwd: data.pwd
    }
  })
}
